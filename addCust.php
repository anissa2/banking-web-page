<?php 
    include ('db_connect.php');
    session_start();
    
    if (!isset($_SESSION['admin_login'])) {
        header('Location: ./admin_login.php');
    }
    
    //get data from form 
    $fname = $_POST['fname'];
    $lname = $_POST['lname'];
    $gender = $_POST['gender'];
    $dob = $_POST['dob'];
    $address = $_POST['address'];
    $mobile = $_POST['mobile'];
    $username = $_POST['username'];
    $inputPass = $_POST['pwd'];
    $acctStatus = $_POST['acctStatus'];
    $primaryAccount = $_POST['primaryAccount'];
    $secondaryAccount = $_POST['secondaryAccount'];
    
    $pwd = md5($inputPass); //encryption  
    
    if (isset($_POST['submit'])) {
        //insert into database
        $stmt = $dbh->prepare("INSERT into customer (id, fname, lname, gender, dob, address, mobile, username, 
                                                    pwd, acctStatus, primaryAccount, secondaryAccount) 
                               VALUES ('', '$fname' , '$lname','$gender', '$dob', '$address', '$mobile', '$username', 
                               '$pwd', '$acctStatus', '$primaryAccount', '$secondaryAccount')");
        $stmt->bindParam(':fname', $fname);
        $stmt->bindParam(':lname', $lname);
        $stmt->bindParam(':gender', $gender);
        $stmt->bindParam(':dob', $dob);
        $stmt->bindParam(':address', $address);
        $stmt->bindParam(':mobile', $mobile);
        $stmt->bindParam(':username', $username);
        $stmt->bindParam(':pwd', $pwd);
        $stmt->bindParam(':acctStatus', $acctStatus);
        $stmt->bindParam(':primaryAccount', $primaryAccount);
        $stmt->bindParam(':secondaryAccount', $secondaryAccount);
    
        $stmt->execute();
        header('location:./listCust.php');
    }
    
    
?>   

<!--add staff form-->    
<html>
    <h2>Customer Form</h2>
        <form action="addCust.php" class="pure-form pure-form-aligned" method="post">
            <div class="pure-control-group">
                <label for="fname">Fisrt Name</label>    <!--firstname--> 
                    <input id="fname" type="text" name="fname"/> <br/> </br>
                
                <label for="lname">Last Name</label>     <!--last name -->
                    <input id="lname" type="text" name="lname"/> </br> </br>
                
                <label for="gender">Gender</label>       <!--gender-->
                    <select name="gender" id="gender"> </br> </br>
                        <option value="M">M</option>
                        <option value="F">F</option>
                    </select> </br> </br>
                
                <label for="dob">Date of Birth</label>  <!--dob--> 
                    <input id="dob" type="text" name="dob" placeholder="YYYY-MM-DD"/> </br> </br>

                <label for="address">Address</label>    <!--address-->
                    <input id="address" type="text" name="address"> <br/> </br>
                
                <label for"mobile">Mobile</label>     <!--mobile-->
                    <input id="mobile" type="text" name="mobile"> </br> </br>
                
                <label for"username">Username</label>     <!--username-->
                    <input id="username" type="text" name="username" placeholder="create username"> </br> </br>
                
                <label for"pwd">Password</label>     <!--password-->
                    <input id="pwd" type="password" name="pwd" placeholder="create password"> </br> </br>
                
                <label for"acctStatus">Account Status</label>     <!--status-->
                    <select name="acctStatus" id="acctStatus"> </br> </br>
                        <option value="active">ACTIVE</option>
                        <option value="nonActive">NON ACTIVE</option>
                    </select> </br> </br>
                
                 <label for="primaryAccount">Primary Account</label>  <!--account--> 
                     <select name="primaryAccount" id="primaryAccount"> </br> </br>
                        <option value="savings">savings</option>
                        <option value="checkings">checkings</option>
                    </select> </br> </br>
                    
                 <label for="secondaryAccount">Secondary Account</label>  <!--account--> 
                     <select name="secondaryAccount" id="secondaryAccount"> </br> </br>
                        <option value="savings">savings</option>
                        <option value="checkings">checkings</option>
                    </select> </br> </br>
                    
                    <input name="submit" value="Submit" type="submit">
                </div>
            </form>
            <p><a href="./admin.html">Go Back</a></p>
</html>
