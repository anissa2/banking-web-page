<style>
    table {
        border-collapse: collapse;
        width: 80%;
        
    }
    th, td {
        text-align: left;
        padding: 8px;
        border-bottom: 1px solid #ddd;
    }
    th {
        background-color: blue;
        color: white;
    }
    
</style>
<h2>List of Staff</h2>
<?php

    include ('db_connect.php');
    
     session_start();
    
    if (!isset($_SESSION['admin_login'])) {
        header('Location: ./admin_login.html');
    }
    
    
    $stmt = $dbh->prepare("SELECT id, fname, lname, gender, dob, position, address, mobile, username FROM staff");
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC); 
    
    echo("<table>");
    echo("<tr> <th>ID</th> 
                <th>First Name</th> 
                <th>Last Name</th> 
                <th>Gender</th> 
                <th>Date of Birth</th> 
                <th>Position</th>
                <th>Address</th>
                <th>Mobile</th>
                <th>Username</th> </tr>");
        foreach($result as $row) {
            echo("<tr>");
            echo("<td>".$row['id']."</td>".
                "<td>".$row['fname']."</td>".
                "<td>".$row['lname']."</td>".
                "<td>".$row['gender']."</td>".
                "<td>".$row['dob']."</td>".
                "<td>".$row['position']."</td>".
                "<td>".$row['address']."</td>".
                "<td>".$row['mobile']."</td>".
                "<td>".$row['username']."</td>"); 
            echo("</tr>");
        }
    echo("</table>");
    echo("<p><a href='./admin.html'>Go Home</a></p>");
?>    
