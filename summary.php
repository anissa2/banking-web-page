<!DOCTYPE>
<header>
<style>
   
    table {
        border: 1px solid black;
        position: relative;
        top: 5px;
    }
    th, td {
        border: 1px solid black;
        padding: 10px;
        
    }
  
</style>
</header>

<h2 class="title">Account Summary</h2>

<?php
    include('db_connect.php');
    session_start();
    $username = $_SESSION['customer_login']; 
    
    if(!isset($_SESSION['customer_login'])) {
        header('Location: ./customer_login.html');
    }
    
    
    $stmt = $dbh->prepare("SELECT acctStatus, savBal, checkBal, balance FROM customer where username = :username");
    $stmt->bindParam('username', $username);
    $result = $stmt->execute();
    $row = $stmt->fetch();
    
    $accountStatus = $row['acctStatus'];
    $savings = $row['savBal'];
    $checkings = $row['checkBal'];
    $totalBalance = $row['balance'];
    
    echo('<table>');
    
        echo('<tr>
            <th>Account Status</th>
            <th>Checkings</th>
            <th>Savings</th>
            <th>Total Balance</th>
            
        </tr>');
        
        echo('<tr>');
            echo('<td>'.$accountStatus.'</td>'.
                '<td>'. $checkings.'</td>'.
                '<td>'. $savings.'</td>'.
                '<td>'. $totalBalance.'</td>');
        echo('</tr>');
    echo('</table>');
    
   echo("<p><a href='./customer.html'>Go Back</a></p>");
    
?>

    
    
    

