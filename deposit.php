<?php
    include('db_connect.php');
    session_start();
    $username = $_SESSION['customer_login'];
   
    if (!isset($_SESSION['customer_login'])) {
        header('Location: ./customer_login.html');
    }
    
    //select current balance from logged in user
    $stmt = $dbh->prepare('SELECT savBal, checkBal, balance FROM customer where username = :username');
    $stmt->bindParam('username', $username);
    $stmt->execute() or exit('Select failed');
    $row = $stmt->fetch() or exit('Fetch failed');
    
    $currentBalance = $row["balance"];
    $currentSavBal = $row["savBal"];
    $currentCheckBal = $row["checkBal"];
    $amount = $_POST['deposit'];
    $savings = 'savings';
    
    if (isset($_POST['submit'])) {
        foreach ($_POST['accountChoice'] as $select) {
                if ($select == $savings) {
                    $savBal = $currentSavBal + $amount;
                    $balance = $currentBalance + $amount;
                    $stmt = $dbh->prepare('UPDATE customer SET savBal = :savBal, balance = :balance where username = :username');
                    $stmt->bindParam('username', $username);
                    $stmt->bindParam(':savBal', $savBal);
                    $stmt->bindParam(':balance', $balance);
                    $stmt->execute() or exit('Select failed');
                } else {
                    $checkBal = $currentCheckBal + $amount;
                    $balance = $currentBalance + $amount;
                    $stmt = $dbh->prepare('UPDATE customer SET checkBal = :checkBal, balance = :balance where username = :username');
                     $stmt->bindParam('username', $username);
                    $stmt->bindParam(':checkBal', $checkBal);
                    $stmt->bindParam(':balance', $balance);
                    $stmt->execute() or exit('Select failed');
                }
                    
            }
        //fetch the new balances
        $stmt = $dbh->prepare('SELECT savBal, checkBal, balance FROM customer where username = :username');
        $stmt->bindParam('username', $username);
        $stmt->execute() or exit('Select failed');
        $row = $stmt->fetch() or exit('Fetch failed');
        $currentBalance = $row["balance"];
        $currentSavBal = $row["savBal"];
        $currentCheckBal = $row["checkBal"];
        
    }
?>
<html>
     <h1>Deposit</h1>
     <?php echo("<h3>Checkings Balance:</h3>"); echo('$'.$currentCheckBal);?>
     <?php echo("<h3>Savings Balance:</h3>"); echo('$'.$currentSavBal);?> </br> </br>
     <?php echo("<h3>Total Balance:</h3>"); echo('$' .$currentBalance);?> </br> </br>
     
     <form action="deposit.php" method="post">
          <label for"accountChoice">Choose Account</label>     <!--status-->
               <select name="accountChoice[]"> </br> </br>
                    <option value="savings" id="savings">savings</option>
                    <option value="checkings" id="checkings">checkings</option>
               </select> </br> </br>  
        
               <label for="amount">Amount:</label>  <!--amount--> 
               <input id="deposit" type="text" name="deposit" placeholder="enter amount"/> </br> </br>
          <input name="submit" value="Submit" type="submit">
     </form>
     
     <p><a href="./customer.html">Go Back</a></p>
</html>
