<?php

    include('db_connect.php');
    //start session
    session_start();

    //extract form data from HTTP request message, set variables for user and password
    $username   = $_POST['username'];
    $submittedPassword = $_POST['password'];

    //check for invalid input
    if (!isset($username) or !isset($submittedPassword)) {
        header('Location: ./admin_login.html');
        exit();
    }

    //prepared statement using SQL select to retrieve password from the database for given username
    $stmt = $dbh->prepare("SELECT password FROM admin WHERE username = :username");
    $stmt->bindParam(':username', $username);
    $stmt->execute();

    //if there are 0 rows returned means there is no such user, redirect to login page
    if ($stmt->rowCount() == 0) {
        header('Location: ./admin_login.html');
        exit();
    }

    //fecth first row from result set and access the string from password column
    $row = $stmt->fetch() or exit('Fetch failed');
    $actualPassword = $row["password"];

    if ($actualPassword != $submittedPassword) {
        header('Location: ./admin_login.html');
        exit();
    }

    $_SESSION['admin_login'] = $username;

    header('Location: ./admin.html');
?>


