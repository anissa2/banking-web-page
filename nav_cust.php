<!DOCTYPE html>
<html>
<head>
<style>
    body {
        margin: 0;
    }

    ul {
        list-style-type: none;
        margin: 0;
        padding: 0;
        width: 15%;
        background-color: #f1f1f1;
        position: fixed;
        height: 100%;
        overflow: auto;
    }

    li a {
        display: block;
        color: #000;
        padding: 8px 16px;
        text-decoration: none;
    }
    
    .feature {
        text-align: center;
        font-weight: bold;
        font-size: 25px;
        text-decoration: underline;
        padding: 45px 0;
        margin-left:15px;
	    margin-bottom:1px;
        
    }

    li a.active {
        background-color: #4CAF50;
        color: white;
    }

    li a:hover:not(.active) {
        background-color: #555;
        color: white;
    }
</style>
</head>
<body>
    
    <ul>
      <li class="feature">Accounts</li>
      
      <li><a href="./summary.php">Account Summary</a></li> <!--summary of customer account balance, status--> 
      
      <li class="feature">Features</li>
      
      <li><a href="./deposit.php">Deposit</a></li>  
      <li><a href="./transfer.php">Transfer</a></li> 
      
      <li class="feature">Profile</li>
      
      <li><a href="./custdetails.php">Personal Details</a></li>  
      <li><a href="./cust_change_pass.php">Change Password</a></li>
      <li><a href="./logout.php">Logout</a></li>
    </ul>
    
    
</body>
</html>
    