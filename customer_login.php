<?php

    include('db_connect.php');
    //start session
    session_start();

    //extract form data from HTTP request message, set variables for user and password
    $username   = $_POST['username'];
    $submittedPassword = md5($_POST['pwd']);

    //check for invalid input
    if (!isset($username) or !isset($submittedPassword)) {
        header('Location: ./customer_login.html');
        echo("Please input username");
        exit();
    }

    //prepared statement using SQL select to retrieve password from the database for given username
    $stmt = $dbh->prepare("SELECT pwd, id FROM customer WHERE username = :username");
    $stmt->bindParam(':username', $username);
    $stmt->execute();

    //if there are 0 rows returned means there is no such user, redirect to login page
    if ($stmt->rowCount() == 0) {
        header('Location: ./customer_login.html');
        echo("user not found");
        exit();
    }

    //fecth first row from result set and access the string from password column
    $row = $stmt->fetch() or exit('Fetch failed');
    $actualPassword = $row["pwd"];
    $customerId = $row['id'];

    if ($actualPassword != $submittedPassword) {
        header('Location: ./customer_login.html');
        echo("wrong password");
        exit();
    }

    $stmt = $dbh->prepare("SELECT id from passBook41 WHERE id = :customerId");
    $stmt->bindParam(':customerId', $id);
    $stmt->execute();
    $session_id = $row['id'];
    
    $_SESSION['customer_login'] = $username;
    
   // echo($session_id);

    header('Location: ./customer.html');
?>
