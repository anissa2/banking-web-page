<?php 
        include ('db_connect.php');
        
        include ('listStaff.php');
?>

<h2>Delete Staff</h2>
<!DOCTYPE>
<html>
    <form action="deleteStaff.php" method="post">
    Delete <input type="text" placeholder="type staff identification" name="id"/>
        <input type="submit" value="Delete" name="delete"/>
    </form>
   
</html>

<?php
if (isset($_POST['delete'])) {
    
        $id = $_POST['id'];
        
        $stmt = $dbh->prepare("DELETE FROM staff WHERE id = :id");
        $stmt->bindParam(':id', $id);
        $delete = $stmt->execute();
        
        if ($delete) {
            header('location: ./deleteStaff.php');
        } else  {
            echo("something went wrong");
        }
}
?>