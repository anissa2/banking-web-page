USE banking;
DROP TABLE IF EXISTS admin 

CREATE TABLE admin (
  id int AUTO_INCREMENT,
  fname varchar(255) NOT NULL,
  lname varchar(255) NOT NULL,
  gender char(1) NOT NULL,  
  dob varchar(255) NOT NULL,
  department varchar(255) NOT NULL,
  address varchar(255) NOT NULL,
  teleNo varchar(11) NOT NULL,
  username varchar(255) NOT NULL,
  pwd varchar(255)NOT NULL,
  Primary Key (id)
  );
  
INSERT INTO admin (id, fname, lname, gender, dob, department, address, teleNo, username, pwd)
VALUES ('1', 'John', 'Smith', 'M', '09/15/1985', 'developer', '3344 Sydney St.', '111-222-3333', 'admin1', 'jsmith01');

DROP TABLE IF EXISTS staff
CREATE TABLE staff ( 
  id int AUTO_INCREMENT,
  fname varchar(255) NOT NULL,
  lname varchar(255) NOT NULL,
  gender char(1) NOT NULL,  
  dob varchar NOT NULL,
  position varchar(255) NOT NULL,
  address varchar(255) NOT NULL,
  mobile varchar(11) NOT NULL,
  username varchar(255) NOT NULL,
  pwd varchar(255) NOT NULL,
  Primary Key (id)
);

/*Table structure for table `customers` */
DROP TABLE IF EXISTS customer;

CREATE TABLE customer ( 
  id int AUTO_INCREMENT,
  fname varchar(255) NOT NULL,
  lname varchar(255),
  gender char(1) NOT NULL,  
  dob varchar(255) NOT NULL,
  address varchar(255) NOT NULL,
  mobile varchar(32) NOT NULL,
  username varchar(255) NOT NULL,
  password varchar(255) NOT NULL,
  acctStatus varchar(255) NOT NULL,
  primaryAccount varchar(255) NOT NULL,
  secondaryAccount varchar(255) NOT NULL,
  savBal int(10) NOT NULL,
  checkBal int(10) NOT NULL,
  balance int(10) NOT NULL,
  Primary Key (id)
);

INSERT INTO customer (id, fname, lname, gender, dob, address, mobile, username, password, acctStatus, primaryAccount, secondaryAccount, savBal, checkBal, balance)
VALUES ('1', 'Jim', 'Halpert', 'M', '01/27/1975', '789 Apple st.', '444-555-6666', 'jhalpert', '1234', 'Active', 'checkings', 'savings', '600', '300', '900');

