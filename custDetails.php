<?php
    session_start();
    include('db_connect.php');
    
    if(!isset($_SESSION['cust_login'])) {
        header('Location: ./cust_login.php');
    }
    
    $stmt = $dbh->prepare('SELECT * from customer where username = :username');
    $stmt->execute() or exit('Select failed');
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC); 
    
    echo("<table>");
        foreach($result as $row) {
            echo("<h1>Personal Details</h1> </br>"); 
            echo("<h3><u>First Name:</u></h3>". $row['fname']);
            echo("<h3><u>Last Name:</u></h3>". $row['lname']);
            echo("<h3><u>Gender:</u></h3>". $row['gender']);
            echo("<h3><u>Address:</u></h3>". $row['address']);
            echo("<h3><u>Mobile:</u></h3>". $row['mobile']);
            echo("<h3><u>username:</u></h3>". $row['username']);
            echo("<h3><u>Account Status:</u></h3>". $row['acctStatus']);
            echo("<h3><u>Primary Account:</u></h3>". $row['primaryAccount']);
            echo("<h3><u>Secondary Account:</u></h3>". $row['secondaryAccount']);
            
            
        }
        echo("</table>");   

?>