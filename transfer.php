<?php
    include('db_connect.php');
    session_start();
    $username = $_SESSION['customer_login']; 
    
    if(!isset($_SESSION['customer_login'])) {
        header('Location: ./customer_login.html');
    }
    
    $stmt = $dbh->prepare('SELECT savBal, checkBal FROM customer where username = :username');
    $stmt->bindParam('username', $username);
    $result = $stmt->execute() or exit('Select failed');
    $row = $stmt->fetch() or exit('Fetch failed');
    
    
    
    $currentSavBal = $row["savBal"];
    $currentCheckBal = $row["checkBal"];
    $amount = $_POST['deposit'];
    $savings = 'savings';
    $checkings = 'checkings';
    
    if (isset($_POST['submit'])) {
        foreach ($_POST['accountChoice1'] as $select) {
            if ($select == $savings) {
                $Isenough = $currentSavBal - $amount; //check if there are enough funds
                if($Isenough > 0) {
                     $checkBal = $currentCheckBal + $amount; //transfer to checkings
                     $savBal = $Isenough; //subtract amount and have new balance
                } else { 
                     echo("Not enough funds");
                }
            $stmt = $dbh->prepare('UPDATE customer SET savBal = :savBal, checkBal = :checkBal where username = :username');
             $stmt->bindParam('username', $username);
            $stmt->bindParam(':checkBal', $checkBal);
            $stmt->bindParam(':savBal', $savBal);
            $stmt->execute() or exit('Select failed');
            } else if ($select == $checkings) {
                $Isenough = $currentCheckBal - $amount;
                if($Isenough > 0) {
                    $savBal = $currentSavBal + $amount;
                    $checkBal = $Isenough; 
                } else { 
                     echo("Not enough funds");
                }
                $stmt = $dbh->prepare('UPDATE customer SET savBal = :savBal, checkBal = :checkBal where username = :username');
                $stmt->bindParam('username', $username);
                $stmt->bindParam(':savBal', $savBal);
                $stmt->bindParam(':checkBal', $checkBal);
                $stmt->execute() or exit('Select failed');
            } else {
                echo("Please select an account");
            }
            
        }
    }
    
    $stmt = $dbh->prepare('SELECT savBal, checkBal, balance FROM customer where username = :username');
    $stmt->bindParam('username', $username);
    $stmt->execute() or exit('Select failed');
    $row = $stmt->fetch() or exit('Fetch failed');
    $currentBalance = $row["balance"];
    $currentSavBal = $row["savBal"];
    $currentCheckBal = $row["checkBal"];
    
?>

<html>
     <h1>Transfer</h1>
     <?php echo("<h3>Checkings Balance:</h3>"); echo('$'.$currentCheckBal);?>
     <?php echo("<h3>Savings Balance:</h3>"); echo('$'.$currentSavBal);?> </br> </br>
     
     <form action="transfer.php" method="post">
          <label for"accountChoice">Transfer From: </label>     <!--status-->
               <select name="accountChoice1[]"> </br> </br>
                    <option value="savings" id="savings">savings</option>
                    <option value="checkings" id="checkings">checkings</option>
               </select> </br> </br> 
               
        <label for"accountChoice">Transfer To: </label>     <!--status-->
               <select name="accountChoice2[]"> </br> </br>
                    <option value="savings" id="savings">savings</option>
                    <option value="checkings" id="checkings">checkings</option>
               </select> </br> </br> 
        
               <label for="amount">Amount:</label>  <!--amount--> 
               <input id="deposit" type="text" name="deposit" placeholder="enter amount"/> </br> </br>
          <input name="submit" value="Transfer" type="submit">
     </form>
      <p><a href="./customer.html">Go Back</a></p>
</html>