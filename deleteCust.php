<?php 
        include ('db_connect.php');
        session_start();
        
        if(!isset($_SESSION['admin_login'])) {
           header('Location: ./admin_login.php'); 
        }
        
        include ('listCust.php');
        
?>

<h2>Delete Customer</h2>
<!DOCTYPE>
<html>
    <form action="deleteCust.php" method="post">
    Delete <input type="text" placeholder="type customer identification" name="id"/>
        <input type="submit" value="Delete" name="delete"/>
    </form>
     <p><a href="./admin.html">Go Back</a></p>
</html>

<?php
if (isset($_POST['delete'])) {
    
        $id = $_POST['id'];
        
        $stmt = $dbh->prepare("DELETE FROM customer WHERE id = :id");
        $stmt->bindParam(':id', $id);
        $delete = $stmt->execute();
        
        if ($delete) {
            echo("Delete Succesful");
            header('location: deleteCust.php');
        } else  {
            echo("something went wrong");
        }
}
?>