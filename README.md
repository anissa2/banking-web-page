# Banking Web Page

This is a banking web page project. There are two users administrator and customer. 
An administrator logs in and can add, edit and delete staff or customers. 
A customer can check their balances and transfer money between accounts. Each user has their 
own tab and login interface. All passwords in this project are encrypted using php's md5() hash function.
There is one database used that holds tables for staff, customer, administrator and passBook which belongs
to each customer identified by the customers id. 
