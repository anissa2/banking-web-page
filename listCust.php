<style>
    table {
        border-collapse: collapse;
        width: 80%;
        text-align: center;
        
    }
    th, td {
        text-align: left;
        padding: 8px;
        border-bottom: 1px solid #ddd;
    }
    th {
        background-color: blue;
        color: white;
    }
    
</style>
<h2 style="text-align:center;">List of Customer</h2>
<?php

    include ('db_connect.php');
    /*
    if(!isset($_SESSION['admin_login'])) {
           header('Location: ./admin_login.php'); 
        }
    */
    
    $stmt = $dbh->prepare("SELECT id, fname, lname, gender, dob, address, mobile, 
                          username, acctStatus, primaryAccount, secondaryAccount FROM customer");
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC); 
    
    echo("<table>");
    echo("<tr> <th>ID</th> 
                <th>First Name</th> 
                <th>Last Name</th> 
                <th>Gender</th> 
                <th>Date of Birth</th> 
                <th>Address</th>
                <th>Mobile</th>
                <th>Username</th> 
                <th>Status</th>
                <th>Primary Account</th>
                <th>Secondary Account</th></tr>");
        foreach($result as $row) {
            echo("<tr>");
            echo("<td>".$row['id']."</td>".
                "<td>".$row['fname']."</td>".
                "<td>".$row['lname']."</td>".
                "<td>".$row['gender']."</td>".
                "<td>".$row['dob']."</td>".
                "<td>".$row['address']."</td>".
                "<td>".$row['mobile']."</td>".
                "<td>".$row['username']."</td>".
                "<td>".$row['acctStatus']."</td>".
                "<td>".$row['primaryAccount']."</td>".
                "<td>".$row['secondaryAccount']."</td>"); 
            echo("</tr>");
        }
    echo("</table>");
?>    