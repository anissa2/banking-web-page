<?php 
    include ("db_connect.php");
    
    session_start();
    
    if (!isset($_SESSION['admin_login'])) {
        header('Location: ./admin_login.html');
    }
    if(isset($_POST['submit'])) {
    
    //get data from form 
    $fname = $_POST['fname'];
    $lname = $_POST['lname'];
    $gender = $_POST['gender'];
    $dob = $_POST['dob'];
    $position = $_POST['position'];
    $address = $_POST['address'];
    $mobile = $_POST['mobile'];
    $username = $_POST['username'];
    $pwd = $_POST['pwd'];
    
    $securePwd = md5($pwd);  //encryption  
    
    //insert into database
    $stmt = $dbh->prepare("INSERT into staff VALUES ('', '$fname' , '$lname','$gender', '$dob', '$position', '$address', '$mobile', '$username', '$securePwd')");
    $stmt->bindParam(':fname', $fname);
    $stmt->bindParam(':lname', $lname);
    $stmt->bindParam(':gender', $gender);
    $stmt->bindParam(':dob', $dob);
    $stmt->bindParam(':position', $position);
    $stmt->bindParam(':address', $address);
    $stmt->bindParam(':mobile', $mobile);
    $stmt->bindParam(':username', $username);
    $stmt->bindParam(':pwd', $pwd);
    $stmt->execute();
    header('location:./listStaff.php');
    }
    
?>   

<!--add staff form-->    
<html>
    <h2>Staff Form</h2>
        <form action="addStaff.php" class="pure-form pure-form-aligned" method="post">
            <div class="pure-control-group">
                <label for="fname">Fisrt Name</label>    <!--firstname--> 
                    <input id="fname" type="text" name="fname"/> <br/> </br>
                <label for="lname">Last Name</label>     <!--last name -->
                    <input id="lname" type="text" name="lname"/> </br> </br>
                <label for="gender">Gender</label>       <!--gender-->
                    <select name="gender" id="gender"> </br> </br>
                        <option value="M">M</option>
                        <option value="F">F</option>
                    </select> </br> </br>
                <label for="dob">Date of Birth</label>  <!--dob--> 
                    <input id="dob" type="text" name="dob" placeholder="YYYY-MM-DD"/> </br> </br>
                <label for="position">Position</label>  <!--position--> 
                    <input id="position" type="text" name="position"/> </br> </br>
                <label for="address">Address</label>    <!--address-->
                    <input id="address" type="text" name="address"> <br/> </br>
                <label for"mobile">Mobile</label>     <!--mobil-->
                    <input id="mobile" type="text" name="mobile"> </br> </br>
                <label for"username">Username</label>     <!--username-->
                    <input id="username" type="text" name="username" placeholder="create username"> </br> </br>
                <label for"pwd">Password</label>     <!--password-->
                    <input id="pwd" type="password" name="pwd" placeholder="create password"> </br> </br>
                    
                    <input name="submit" value="Submit" type="submit">
                    </div>
            </form>
            
</html>

   
    
    